#!/bin/sh

PHP_DISPLAY_ERRORS=Off
PHP_MAX_EXECUTION_TIME=${PHP_MAX_EXECUTION_TIME:-30}
PHP_MAX_FILE_UPLOADS=${PHP_MAX_FILE_UPLOADS:-20}
PHP_MAX_POST_SIZE=${PHP_MAX_POST_SIZE:-8M}
PHP_MAX_UPLOAD_FILESIZE=${PHP_MAX_UPLOAD_FILESIZE:-2M}
if test -z "$PHP_MEMORY_LIMIT"; then
    PHP_MEMORY_LIMIT=-1
fi
if test -z "$PHP_ERRORS_LOG"; then
    PHP_ERRORS_LOG=/proc/self/fd/2
fi
if test "$DEBUG"; then
    PHP_DISPLAY_ERRORS=On
fi

sed -i \
    -e "s/display_errors = .*/display_errors = $PHP_DISPLAY_ERRORS/" \
    -e "s|;error_log = syslog|error_log = $PHP_ERRORS_LOG|" \
    -e "s/max_execution_time = .*/max_execution_time = $PHP_MAX_EXECUTION_TIME/" \
    -e "s/max_file_uploads = .*/max_file_uploads = $PHP_MAX_FILE_UPLOADS/" \
    -e "s/memory_limit = .*/memory_limit = $PHP_MEMORY_LIMIT/" \
    -e "s/post_max_size = .*/post_max_size = $PHP_MAX_POST_SIZE/" \
    -e "s/upload_max_filesize = .*/upload_max_filesize = $PHP_MAX_UPLOAD_FILESIZE/" \
    /usr/local/etc/php/php.ini

if test -s /usr/local/lib/php/extensions/no-debug-non-zts-20190902/redis.so \
	-a "$PHP_REDIS_HOST"; then
    PHP_REDIS_PORT=${PHP_REDIS_PORT:-6379}
    cat <<EOF >>/usr/local/etc/php/php.ini
redis.session.locking_enabled = 1
redis.session.lock_retries = 4000
session.save_handler = redis
session.save_path = tcp://$PHP_REDIS_HOST:$PHP_REDIS_PORT
EOF
fi
