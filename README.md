# k8s PHP

Generic Apache+PHP image.

Forked from https://github.com/docker-library/php

Diverts from https://gitlab.com/synacksynack/opsperator/docker-apache

Build with:

```
$ make build
```

Run with:
```
$ make run
```

Build in OpenShift:

```
$ make ocbuild
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name           |    Description             | Default                                                     | Inherited From    |
| :------------------------- | -------------------------- | ----------------------------------------------------------- | ----------------- |
|  `APACHE_DOMAIN`           | Apache ServerName          | `example.com`                                               | opsperator/apache |
|  `APACHE_IGNORE_OPENLDAP`  | Ignore LemonLDAP autoconf  | undef                                                       | opsperator/apache |
|  `APACHE_HTTP_PORT`        | Apache Listen Port         | `8080`                                                      | opsperator/apache |
|  `ONLY_TRUST_KUBE_CA`      | Don't trust base image CAs | `false`, any other value disables ca-certificates CAs       | opsperator/apache |
|  `OPENLDAP_BASE`           | OpenLDAP Base              | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX` | OpenLDAP Bind DN Prefix    | `cn=apache,ou=services`                                     | opsperator/apache |
|  `OPENLDAP_BIND_PW`        | OpenLDAP Bind Password     | `secret`                                                    | opsperator/apache |
|  `OPENLDAP_CONF_DN_RREFIX` | OpenLDAP Conf DN Prefix    | `cn=lemonldap,ou=config`                                    | opsperator/apache |
|  `OPENLDAP_DOMAIN`         | OpenLDAP Domain Name       | undef                                                       | opsperator/apache |
|  `OPENLDAP_HOST`           | OpenLDAP Backend Address   | undef                                                       | opsperator/apache |
|  `OPENLDAP_PORT`           | OpenLDAP Bind Port         | `389` or `636` depending on `OPENLDAP_PROTO`                | opsperator/apache |
|  `OPENLDAP_PROTO`          | OpenLDAP Proto             | `ldap`                                                      | opsperator/apache |
|  `PHP_ERRORS_LOG`          | PHP Errors Logs Output     | `/proc/self/fd/2`                                           |                   |
|  `PHP_MAX_EXECUTION_TIME`  | PHP Max Execution Time     | `30` seconds                                                |                   |
|  `PHP_MAX_FILE_UPLOADS`    | PHP Max File Uploads       | `20`                                                        |                   |
|  `PHP_MAX_POST_SIZE`       | PHP Max Post Size          | `8M`                                                        |                   |
|  `PHP_MAX_UPLOAD_FILESIZE` | PHP Max Upload File Size   | `2M`                                                        |                   |
|  `PHP_MEMORY_LIMIT`        | PHP Memory Limit           | `-1` (no limitation)                                        |                   |
|  `PHP_REDIS_HOST`          | PHP Redis Sessions Host    | undef                                                       |                   |
|  `PHP_REDIS_PORT`          | PHP Redis Sessions Port    | `6379`                                                      |                   |
|  `PUBLIC_PROTO`            | Apache Public Proto        | `http`                                                      | opsperator/apache |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                                                             | Inherited From    |
| :------------------ | ----------------------------------------------------------------------- | ----------------- |
|  `/certs`           | Apache Certificate (optional)                                           | opsperator/apache |
|  `/vhosts`          | Apache VirtualHosts templates root - processed during container start   | opsperator/apache |
