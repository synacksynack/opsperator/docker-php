FROM opsperator/apache

# Apache+PHP Base image for OpenShift Origin

ARG DO_UPGRADE=
ENV GPG_KEYS="1729F83938DA44E27BA0F4D3DBDB397470D12172 BFDDD28642824F8118EF77909B67A5C12229118F" \
    PHP_ASC_URL="https://www.php.net/distributions/php-8.0.17.tar.xz.asc" \
    PHP_CFLAGS="-fstack-protector-strong -fpic -fpie -O2 -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64" \
    PHP_CPPFLAGS="-fstack-protector-strong -fpic -fpie -O2 -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64" \
    PHP_INI_DIR=/usr/local/etc/php \
    PHP_LDFLAGS="-Wl,-O1 -pie" \
    PHP_SHA256="4e7d94bb3d144412cb8b2adeb599fb1c6c1d7b357b0d0d0478dc5ef53532ebc5" \
    PHP_URL="https://www.php.net/distributions/php-8.0.17.tar.xz" \
    PHP_VERSION=8.0.17

LABEL io.k8s.description="Apache 2.4 & PHP $PHP_VERSION Base Image." \
      io.k8s.display-name="Apache 2.4 & PHP $PHP_VERSION" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="apache,httpd,apache2,apache24,php,php8" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-php" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$PHP_VERSION"

USER root

COPY config/* /usr/local/bin/

RUN set -x \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install PHP Dependencies" \
    && a2dismod mpm_event \
    && a2enmod mpm_prefork \
    && apt-get install -y --no-install-recommends autoconf dpkg-dev file g++ \
	gcc libc-dev make pkg-config re2c ca-certificates curl xz-utils wget \
	dirmngr gnupg apache2-dev build-essential \
    && ( \
	echo 'Package: php*'; \
	echo 'Pin: release *'; \
	echo 'Pin-Priority: -1'; \
    ) >/etc/apt/preferences.d/php \
    && ( \
	echo '<FilesMatch \.php$>'; \
	echo '    SetHandler application/x-httpd-php'; \
	echo '</FilesMatch>'; \
	echo 'DirectoryIndex disabled'; \
	echo 'DirectoryIndex index.php index.html'; \
	echo '<Directory /var/www/>'; \
	echo '    Options -Indexes'; \
	echo '    AllowOverride All'; \
	echo '</Directory>'; \
    ) >/etc/apache2/conf-available/docker-php.conf \
    && a2enconf docker-php \
    && mkdir -p $PHP_INI_DIR/conf.d /usr/src \
    && cd /usr/src \
    && wget -O php.tar.xz "$PHP_URL" \
    && if test "$PHP_SHA256"; then \
	echo "$PHP_SHA256 *php.tar.xz" | sha256sum -c -; \
    elif test "$PHP_ASC_URL"; then \
	wget -O php.tar.xz.asc "$PHP_ASC_URL" \
	&& export GNUPGHOME="$(mktemp -d)" \
	&& for key in $GPG_KEYS; \
	    do \
		gpg --keyserver ha.pool.sks-keyservers.net --recv-keys "$key"; \
	    done \
	&& gpg --batch --verify php.tar.xz.asc php.tar.xz \
	&& if command -v gpgconf >/dev/null; then \
	    gpgconf --kill all; \
	fi \
	&& rm -rf "$GNUPGHOME"; \
    fi \
    && savedAptMark="$(apt-mark showmanual)" \
    && apt-get update \
    && apt-get install -y --no-install-recommends libcurl4-openssl-dev \
	libedit-dev libsodium-dev libsqlite3-dev libssl-dev libxml2-dev \
	zlib1g-dev libargon2-dev libonig-dev libreadline-dev \
    && export CFLAGS="$PHP_CFLAGS" CPPFLAGS="$PHP_CPPFLAGS" \
	LDFLAGS="$PHP_LDFLAGS" \
    && docker-php-source extract \
    && cd /usr/src/php \
    && gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)" \
    && debMultiarch="$(dpkg-architecture --query DEB_BUILD_MULTIARCH)" \
    && if test ! -d /usr/include/curl; then \
	ln -sT "/usr/include/$debMultiarch/curl" /usr/local/include/curl; \
    fi \
    && ./configure --build="$gnuArch" --with-config-file-path="$PHP_INI_DIR" \
	--with-config-file-scan-dir="$PHP_INI_DIR/conf.d" --enable-mysqlnd \
	--enable-option-checking=fatal --with-mhash --enable-ftp --with-curl \
	--with-libedit --enable-mbstring --with-password-argon2 \
	--with-pdo-sqlite=/usr --with-sqlite3=/usr --with-pear --with-zlib \
	--with-sodium=shared --with-openssl --with-readline --with-apxs2 \
	`test "$gnuArch" = s390x-linux-gnu && echo '--without-pcre-jit'` \
	--disable-cgi --with-libdir="lib/$debMultiarch" \
    && make -j "$(nproc)" \
    && make install \
    && find /usr/local/bin /usr/local/sbin -type f -executable -exec strip \
	--strip-all '{}' + || true \
    && make clean \
    && cp -v php.ini-* "$PHP_INI_DIR/" \
    && mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" \
    && cd / \
    && docker-php-source delete \
    && apt-mark auto '.*' >/dev/null \
    && if test "$savedAptMark"; then \
	apt-mark manual $savedAptMark; \
    fi \
    && find /usr/local -type f -executable -exec ldd '{}' ';' \
	| awk '/=>/{print $(NF-1)}' \
	| sort -u \
	| xargs -r dpkg-query --search \
	| cut -d: -f1 \
	| sort -u \
	| xargs -r apt-mark manual \
    && echo "# Fixing permissions" \
    && chown -R 1001:root /usr/local/etc/php \
    && chmod -R g=u /usr/local/etc/php \
    && echo "# Cleaning Up" \
    && apt-get purge -y --auto-remove \
	-o APT::AutoRemove::RecommendsImportant=false \
    && php --version \
    && pecl update-channels \
    && apt-get purge -y --auto-remove \
	-o APT::AutoRemove::RecommendsImportant=false wget dirmngr gnupg \
	apache2-dev \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rf /tmp/pear ~/.pearrc /var/lib/apt/lists/* \
	/usr/share/man /usr/share/doc /usr/local/php/man \
    && /usr/local/bin/docker-php-ext-enable sodium \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
